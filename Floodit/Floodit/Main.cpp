#include <vector>
#include <string> 
#include <iostream> 
#include <cstdlib>
#include <ctime> 
#include "Floodit.h"


int main() {

	std::cout << "Are you ready to start a new game? (Enter yes or no)" << std::endl;

	std::string start_game = "";
	std::cin >> start_game;

	//If the user still wants to play, start a new game. 
	while (start_game != "no") {
		
		//Seed the random number generator 
		srand(std::time(nullptr));

		// Option to let user decide how big the board should be 
		//std::cout << "How big of a board do you want?";
		//int size_of_board = 0; 
		//std::cin >>size_of_board; 
		//std::cout << "How many colors do you want on your board?";
		//int number_of_colors = 0; 
		//std::cin >> number_of_colors
		//Then would create Board1 = FlooditBoard(size_of_board, number_of_colors)

		//Create a board and print it
		FlooditBoard Board1 = FlooditBoard();
		Board1.printBoard();

		size_t maxNumber = Board1.getMaxNumber();

		int number_of_moves = maxNumber * 2; 

		//While the user still has moves and the board has not been flooded, allow the user to choose another color to flood 
		while (number_of_moves > 0 && Board1.board_flooded() == false) {
			std::cout << "You have " << number_of_moves << " moves remaining." << std::endl; 
			int number_to_flood = -1;

			// Make sure that the user enters in a number in the valid range, since the flooding process is long, so we don't 
			// want to have to go through it unless it is necessary to do so. 
			while (number_to_flood < 0 || number_to_flood > maxNumber) {
				std::cout << "Please enter a number between 0 and " << maxNumber << ": ";
				std::cin >> number_to_flood;
			}

			//Decide which elements to flood and then print the board 
			Board1.floodBoard(number_to_flood);
			Board1.printBoard(); 

			//Decrement the number of moves left
			--number_of_moves; 
		}

		//if the while loop above was exited because the board was flooded then the user wins; if it was exited because the user
		//ran out of moves, then the user loses. 
		if (Board1.board_flooded())
			std::cout << "Congratulations! You won!" << std::endl;
		else
			std::cout << "Sorry... you ran out of moves. Game over." << std::endl;

		std::cout << "Do you want to play again? (Enter yes or no)" << std::endl; 
		std::cin >> start_game; 
	}
	
	//If the user wants to stop playing or opened this program by accident and wants to close it. 
	std::cout << "Thanks for playing! Quitting program now." << std::endl;
	
	return 0;
}