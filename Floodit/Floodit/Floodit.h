#include <vector>
#include <iostream>
#include <cstdlib>
#include <cmath>


class FlooditBoard {
private:
	size_t size;
	size_t num_of_colors; 
	std::vector<int> board; 
	std::vector<int> flooded_indices;

public:
	/** 
	The default constructor should, for each size given, construct a vector that holds n*n ints (all 0's). 
	Then it should assign a random number between 1 and number_of_colors to each space in the vector; 
	this number will later determine the color. 
	The vector called flooded_indices should contain the indices of the elements of the board that have been flooded; 
	at the beginning, this should only contain the top left corner element (the element of index 0) and any element around it 
	which shares the same number. 
	*/
	FlooditBoard(size_t size_of_board = 4, size_t number_of_colors = 4) : size(size_of_board), num_of_colors(number_of_colors), 
		board(std::vector<int>(size*size, 0)) {
		int x = 0; 
		if (size > 1) {
			for (size_t i = 0; i < size*size; i++) {
				x = static_cast<int>(rand() % num_of_colors);
				board[i] = x;
			}
			flooded_indices = { 0 }; 
			floodBoard(board[0]); 
		}
	}

	/**
	The printBoard function will go through and print all the ints in the vector board, using |'s to separate the ints 
	and newlines to indicate the structure of the grid. 
	A sample output might be: 
	3 | 1 | 4 | 1
	5 | 9 | 2 | 6
	5 | 3 | 3 | 8
	3 | 2 | 7 | 9
	assuming that size of the board is 4 and the number of colors is 10. 
	*/
	void printBoard() const {
		size_t size_counter = 0;
		for (auto& x : board) {
			++size_counter;
			std::cout << x; 
			if (size_counter%size == 0) {
				std::cout << std::endl;
			}
			else
				std::cout << " | "; 
		}
	}

	// This function should take in an int and check which elements in the board member variable share that value and 
	// are "next to" an element that has already been flooded; these elements should be added to the vector of indices of flooded
	// elements; note that this is not const because it will modify the Floodit object in some way. 
	void floodBoard(int number_to_flood) {
		std::vector<int> indices_to_check;
		int indices_added = 1; 
		
		while (indices_added > 0) {
			//reset the number of indices_added
			indices_added = 0; 

			for (size_t j = 0; j < flooded_indices.size(); ++j) {
				//for each flooded index, look at the 4 elements adjacent to it in the grid
				int flooded_index = flooded_indices[j];
				int index_left = flooded_index - 1;
				int index_right = flooded_index + 1;
				int index_top = flooded_index - size;
				int index_bottom = flooded_index + size;

				//if these elements exist (i.e. if there is an index in the board vector that corresponds to these elements, 
				//then add them to the vector of indices to check; this vector contains all the indices of elements that could be 
				//flooded, if their value is the correct one
				if (index_left >= 0 && index_left < size*size) {
					indices_to_check.push_back(index_left);
					++indices_added; 
				}
				if (index_right >= 0 && index_right < size*size) {
					indices_to_check.push_back(index_right);
					++indices_added; 
				}
				if (index_top >= 0 && index_top < size*size) {
					indices_to_check.push_back(index_top);
					++indices_added; 
				}
				if (index_bottom >= 0 && index_bottom < size*size) {
					indices_to_check.push_back(index_bottom);
					++indices_added; 
				}
				// look at desired_value + 1, desired_value - 1, desired_value - size, desired_value + size
				// if these are valid indices in the board vector, and if they are not already in indices_to_check, 
				// add them to indices_to_check
			}

			//check the value of each of the indices_to_check in the board vector; if it matches the number_to_flood, then it will 
			//be flooded, so we add it to the vector of flooded indices. 
			for (size_t i = 0; i < indices_to_check.size(); ++i) {
				int index_to_check = indices_to_check[i];
				if (board[index_to_check] == number_to_flood)
					flooded_indices.push_back(index_to_check);
			}
		}
		//now for each value in flooded_indices, change the value of the corresponding entry in the board vector to 
		//number_to_flood; this corresponds to the fact that in a normal Floodit game when you choose a particular color, the 
		//flooded parts of the board all become that color
		for (size_t i = 0; i < flooded_indices.size(); ++i) {
			int flooded_index = flooded_indices[i];
			board[flooded_index] = number_to_flood; 
		}
	}

	// Returns the maximum number on the board; this is a const member function (accessor) since it should not change the board 
	// itself; the return of this function is the largest number that appears on the board 
	size_t getMaxNumber() const {
		return num_of_colors - 1; 
	}

	//Returns whether or not the board has been flooded; if the vector of flooded indices has the same size as the whole board 
	//this should mean that the board is flooded. 
	bool board_flooded() const {
		return flooded_indices.size() == size*size; 
	}
};
