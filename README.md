### What is this repository for? ###

Project idea 1: Algorithm Calculator 
My goal is to create a "calculator" for a series of algorithms, such as computing an x that satisfies the Chinese Remainder 
Theorem for a given series of integers and corresponding remainders, computing the result of the Euclidean Algorithm, etc. 
I will first try to implement this using the console, so that a user can type in which algorithm they want to use and then 
they will be prompted to enter the required parameters for that particular algorithm. The calculator will then give the 
correct result. I will then try to use Qt to make my calculator more of an interactive interface where the user can click on 
one of several buttons at the top to indicate which calculator they want to use instead of having to type that information in; 
after clicking on one of them, the proper input form will appear. 

In implementing this calculator, I want to use as many concepts from class as possible, such as containers. I think that the 
most important aspect of my project will be to work on efficiency and with minimizing runtime especially when working with 
large numbers. 

Project idea 2: Flood It game 
I would like to implement a game where you can first choose the size of the board (and maybe the number of colors you want to
show up on the board). Then a board would be generated where each box is assigned one of the colors randomly. Starting from 
the top left corner of the board, the user can choose which color they would like to flood the board with; if there is a box 
adjacent to a box that the user has already flooded of the desired color, the area that the user has flooded grows by that box
and all the boxes that have been flooded so far change color to the chosen color. The process repeats. For each size of the 
board, the user has a certain number of moves before the game is over. If the user can flood the entire board within that
number of moves, then the user wins; if not, the user loses. 

As of now, I am planning on implementing project idea 2; however, I may change my project at a later time. 

#README 

Contents of this repository: 
- a header file defining/implementing a class FlooditBoard
- a .cpp file which contains the code necessary to run a Floodit game 

How to play:
- At the beginning of the game, the user is presented with an n x n grid of some size n which has m different colors 
- The user has a certain number of moves/turns (calculated from the size of the grid and the number of colors in it) 
in which to "flood" the board 
- Game play starts at the top left corner of the grid, where the element in the top left corner as well as any spaces 
next to it that share the same color (number) are considered "flooded" with the color (number) that they contain
- For each turn, the user chooses a color (in this case, a number) from the colors on the board. If there are any spaces 
next to an already-flooded space that are of this color, then these spaces become flooded as well, and then the board is 
reprinted with all the flooded spaces containing the same color (number)
- The game ends either when the user floods the whole board (user wins) or when the user runs out of moves (user loses) 

The FlooditBoard class 
has 4 member variables 
	- size (if the size is n, then the FlooditBoard object represents an n x n grid)
	- num_of_colors (the number of different colors that will appear on the FlooditBoard)
	- board (a vector of integers between 0 and num_of_colors - 1, where each different integer represents 
	    a different color; this is the board used in the Floodit game)
	- flooded_indices (a vector of integers which stores the indices of all the elements in the board 
		which have been flooded)
has a constructor
	- which gives the default values of 4 and 4 to the size and num_of_colors variables, but can take in values to 
	replace these default ones 
	- initializes the board vector to a vector that holds size*size elements, where each of the elements is a random 
	number between 0 and num_of_colors - 1
	- sets the flooded indices to a vector containing the top left element as well as any of the elements adjacent to 
	it that have the same number(color)
has a printBoard() member function 
	- which prints out the elements in the board vector so that it looks like a grid 
has a floodBoard(int)  member function 
	- which takes in an integer between 0 and num_of_colors - 1 (this integer represents the color the user wants to 
	flood the board with), determines which elements can be flooded with this color, and updates the board and 
	flooded_indices vectors appropriately 
has a getMaxNumber() member function 
	- which returns the largest number that appears in the board vector (that is, num_of_colors - 1)
has a board_flooded() member function 
	- which returns true if the board has been completely flooded (that is, all the elements have the same color) and 
	false otherwise 
	
Coding practices/decisions I made: 
- in the constructor for a FlooditBoard object, I chose to use the random number generator rand() to assign numbers 
to the board vector; although I read online in many places that rand() isn't the best random number generator to use, 
I used it anyway since it only needs to generate small numbers in relatively small amounts, and I did not think that 
my program would run that much better if I used a more precise or more random generator based on a complex algorithm

- I used size_t to index my containers since while in practice my program will not use a vector of significantly large 
size, it could be possible to do so, since my constructor does not place an upper bound on the size of the board vector

- I used a vector to represent my board because it permits random access, so it is easier and less expensive to 
perform all the checking of indices that the floodBoard member function needs to; furthermore, since a lot of work has 
been put into optimizing vectors, I knew that using a vector would enable me to not have to worry about memory issues 
such as asking for additional memory within a try-catch block, since it should be written to obey RAII in the standard

- As of this moment I have not yet implemented a destructor, move constructor, copy constructor, or assignment operator
for my class because my program does not require any of these, although I know in practice I should at least implement 
a virtual destructor to obey RAII
  
Things I would like to improve: 
- the floodBoard member function - currently, it requires a lot of code and it creates a lot of vectors; if possible, 
I would like to find a way to rewrite it so that it is less expensive of a process
- the graphics - this program is currently coded so as to run on the console, but ideally it would be nice to use a 
program like Qt so that the grid actually looks like a grid and the numbers are replaced by colors instead 


